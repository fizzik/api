package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os/user"

	"github.com/FogCreek/mini"
	"github.com/julienschmidt/httprouter"
	_ "github.com/lib/pq"
)

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func params() string {
	u, err := user.Current()
	fatal(err)
	cfg, err := mini.LoadConfiguration(u.HomeDir + "/.configpgsql")
	fatal(err)

	info := fmt.Sprintf("host=%s port=%s dbname=%s "+
		"sslmode=%s user=%s password=%s ",
		cfg.String("host", "127.0.0.1"),
		cfg.String("port", "5432"),
		cfg.String("dbname", u.Username),
		cfg.String("sslmode", "disable"),
		cfg.String("user", u.Username),
		cfg.String("pass", ""),
	)
	return info
}

var db *sql.DB

func main() {

	router := httprouter.New()
	router.GET("/api/v1/task/:id", getRecord)
	router.POST("/api/v1/task", addRecord)
	http.ListenAndServe(":8080", router)
}
