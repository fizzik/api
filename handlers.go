package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

type Input struct {
	Id              int    `json:"id"`
	Payload         string `json:"payload"`
	Hash_rounds_cnt int    `json:"hash_rounds_cnt"`
}

func getID(w http.ResponseWriter, ps httprouter.Params) (int, bool) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		w.WriteHeader(400)
		return 0, false
	}
	return id, true
}

func addRecord(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var rec Input
	err := json.NewDecoder(r.Body).Decode(&rec)
	if err != nil || rec.Payload == "" || rec.Hash_rounds_cnt == "" {
		w.WriteHeader(400)
		return
	}
	if _, err := insert(rec.Payload, rec.Hash_rounds_cnt); err != nil {
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(201)
}



func getRecord(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, ok := getID(w, ps)
	if !ok {
		return
	}
	rec, err := readOne(id)
	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(404)
			return
		}
		w.WriteHeader(500)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if err = json.NewEncoder(w).Encode(rec); err != nil {
		w.WriteHeader(500)
	}
}
