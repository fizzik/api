package main

import (
	"database/sql"
)

func insert(payload, hash_rounds_cnt string) (sql.Result, error) {
	return db.Exec("INSERT INTO tasks VALUES (default, $1, $2)",
		payload, hash_rounds_cnt)
}

func readOne(id int) (Input, error) {
	var rec Input
	row := db.QueryRow("SELECT * FROM tasks WHERE id=$1 ORDER BY id", id)
	return rec, row.Scan(&rec.Id, &rec.Payload, &rec.Hash_rounds_cnt)
}
